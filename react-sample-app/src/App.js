import React from 'react';
import { BrowserRouter,  Routes, Route, Link } from 'react-router-dom';
import Home from "./home.js";
import Page1 from "./page1.js";
import NoMatch from "./nomatch.js";

function App() {
  return (
    <BrowserRouter>
      <h1>Hello React Router</h1>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/page1">About</Link>
          </li>
        </ul>
      <Routes>
        <Route exact path='/' element={<Home />} />
        <Route path='/page1' element={<Page1 />} />
        <Route path="*" element={<NoMatch message="NotFound" />} />
      </Routes>
    </BrowserRouter>
  );
}

// function Home() {
//   return <h2>Home</h2>;
// }

function About() {
  return <h2>About</h2>;
}

function Contact() {
  return <h2>Contact</h2>;
}

export default App;